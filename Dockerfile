ARG GOLANG_TAG='1.21-alpine'
FROM golang:${GOLANG_TAG} as builder
LABEL version="1.0" student="<leetoff1337@gmail.com>" purpose="Otus Microservice Architecture"
WORKDIR /build
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o ./health main.go

FROM scratch
COPY --from=builder /build/health /
EXPOSE 8000

CMD ["/health"]
